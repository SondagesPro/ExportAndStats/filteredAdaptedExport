<?php
/**
 * Description
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 1.6.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
namespace filteredAdaptedExport\exports;

use Yii;
use CException;
use Survey;
use SurveyLanguageSetting;

class filteredExportXls extends filteredExport
{

    protected $filename;

    protected $workbook;
    protected $currentSheet;
    protected $separator;
    protected $rowCounter;
    protected $forceDownload = true;

    /**
     * Star export
     * @param $aHeader
     */
    protected function startExport($aHeader) {
        parent::startExport($aHeader);
        if (intval(App()->getConfig('versionumber') <=3)) {
            require_once(APPPATH.'/third_party/xlsx_writer/xlsxwriter.class.php');
        }
        $this->separator = '~|';
        $this->rowCounter = 0;

        $this->workbook = new \XLSXWriter();
        $this->workbook->setTempDir(Yii::app()->getConfig('tempdir'));
        /* Get name by survey */
        $oSurveyLanguage = SurveyLanguageSetting::model()->findByPk(array(
            'surveyls_survey_id' => $this->surveyId,
            'surveyls_language' => $this->language,
        ));
        $this->filename = \XLSXWriter::sanitize_filename('compiled_'.$oSurveyLanguage->surveyls_title.".xlsx");
        header('Content-disposition: attachment; filename="'.($this->filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $worksheetName = "survey_".$this->surveyId;
        $this->currentSheet = $worksheetName;
        $this->workbook->writeSheetRow($this->currentSheet, $aHeader);
        // Todo : add some decoration

    }

    /**
     * Add data line
     * @param string[] $aData
     * @param array[] $aOperations
     * @param float $weigth
     */
    protected function addExportData($aData) {
        $this->workbook->writeSheetRow($this->currentSheet, $aData);
    }

    /**
     * End the export
     * return array|void
     */
    protected function endExport()
    {
        Yii::import('application.helpers.viewHelper');
        \viewHelper::disableHtmlLogging();
        $this->workbook->writeToStdOut();
        Yii::app()->end();
    }

}
