<?php
/**
 * Description
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2022 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 1.6.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
namespace filteredAdaptedExport\exports;

use Yii;
use CException;
use Survey;

class filteredExport
{
    /* var integer surveyid */
    public $surveyId = 0;
    /* var string|null language */
    public $language;

    /* var string|false $questionCompile column name for compilation */
    public $questionCompile = 'id';
    /* var string|false $questionCompileBy column name for compilation */
    public $questionCompileBy = false;

    /* var string[] $questionExportData column names to export */
    public $questionsExportData = array();
    /* var array[] $questionsExportDataAnswers answer for export data */
    public $questionsExportDataAnswers = array();
    /* var string[] $questionExportCount column names to export */
    public $questionsExportCount = array();
    /* var string[] $questionExportSum column names to export */
    public $questionsExportSum = array();
    /* var string[] $questionExportAverage column names to export */
    public $questionsExportAverage = array();
    /* var string|false $questionWheight column name for weighted compilation */
    public $questionExportWeight = false;
    /* var string[] $questionsExportAverageWeighted column names to export */
    public $questionsExportAverageWeighted = array();
    /* var string|false $questionExportWeightAlt column name for weighted compilation */
    public $questionExportWeightAlt = false;
    /* var string[] $questionsExportAverageWeightedAlt column names to export */
    public $questionsExportAverageWeightedAlt = array();

    /* var string dataHeader : (code|text|none)*/
    public $dataHeader = 'code';

    /* var string exportHeader : (code|text)*/
    public $exportHeader = 'text';
    /* var string exportAnswers : (code|text)*/
    public $exportAnswers= 'text';

    /* var string $token for filtering */
    public $token = '';
    /* var boolean */
    public $completed = true;
    /* var integer[] response id to select*/
    public $responseIds = array();
    /* var string[] exra filter to apply : column=>value */
    public $aFilters = array();
    /* var boolean $none select no response */
    public $none = false;

    /**
     *  var array[] extra options, current are
     * alltotal => string : Y add, N don't add, A : automatic
     * NATextValue : use for Not applicable
     * NotsetTextValue : use in place of null.
     **/
    public $options = array(
        'alltotal' => 'A',
        'NATextValue' => '',
        'NotsetTextValue' => '',
    );

    /* var boolean */
    public $throwError = false;

    /* string[] */
    private $aQuestionColumns = array();

    /* string|null */
    public $languageSource = null;

    /* string[] */
    protected $aHeader = array();
    /* string[] */
    protected $aDatas = array();

    /**
     * conctructor
     * @param Object Plugin extend PluginBase
     * @param integer surveyid
     * @param string language
    **/
    public function __construct($surveyId , $language = null)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if(!$oSurvey) {
            throw new CException("Invalid surveyId");
        }
        if($oSurvey->active != "Y") {
            throw new CException("Invalid survey");
        }
        $this->surveyId = $surveyId;
        if(is_null($language)) {
            $language = $oSurvey->language;
        } elseif (!in_array($language, $oSurvey->getAllLanguages())) {
            throw new CException("Invalid language");
        }
        $this->language = $language;
        $this->aQuestionColumns = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId);
    }

    /**
     * Set the compile question
     * This is the core of the system, extend with care …
     * @throw error
     * @return array|void
     */
    public function export()
    {
        $oSurvey = Survey::model()->findByPk($this->surveyId);
        /* Header */
        $questionsExportData = array_unique(array_merge(
            array(
                $this->questionCompile,
                $this->questionCompileBy,
            ),
            $this->questionsExportData
        ));
        $this->setQuestionsExportDataAnswers($questionsExportData);
        $questionsToBeExported = array_filter(array_merge(
            $questionsExportData,
            $this->questionsExportCount,
            $this->questionsExportSum,
            $this->questionsExportAverage,
            $this->questionsExportAverageWeighted,
            $this->questionsExportAverageWeightedAlt
        ));

        $questionsToBeSelected = array_unique(array_filter(array_merge(
            array('id','submitdate'),
            array(
                $this->questionCompile,
                $this->questionCompileBy,
                $this->questionExportWeight,
                $this->questionExportWeightAlt
            ),
            $questionsToBeExported,
            array_keys($this->aFilters)
        )));
        if ($oSurvey->anonymized != 'Y') {
            $questionsToBeSelected[] = 'token';
        }

        $questionsExportData = array_unique(array_filter($questionsExportData));
        $questionsQuotedToBeSelected = array_map(
            function($column) {
                return \App()->getDb()->quoteColumnName($column);
            },
            $questionsToBeSelected
        );

        /* The header */
        $columnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($this->surveyId,$this->language);
        $allQuestionListData = $columnsInformation->allQuestionListData();
        /* Reorder and header*/
        $aHeader = array();
        $aOperations = array();
        $countValues = array_count_values($questionsToBeExported);
        foreach($allQuestionListData['options'] as $column => $questionListData) {
            $headerDone = false;
            $emCode = $this->aQuestionColumns[$column];
            if (isset($countValues[$column])) {
                if($countValues[$column] == 1) {
                    $aHeader[$emCode] = $questionListData['title'];
                    $headerDone = true;
                }
                if(in_array($column,$questionsExportData)) {
                    $aOperations[] = array(
                        'column' => $column,
                        'op' => 'data',
                        'id' => 'data'.$column
                    );
                    if(!$headerDone) {
                        $aHeader[$emCode] = $questionListData['title'];
                    }
                }
                if(in_array($column,$this->questionsExportCount)) {
                    $aOperations[] = array(
                        'column' => $column,
                        'op' => 'count',
                        'id' => 'count'.$column
                    );
                    if(!$headerDone) {
                        $aHeader['count_'.$emCode] = sprintf($this->translate("Count: %s"), $questionListData['title']);
                    }
                }
                if(in_array($column,$this->questionsExportSum)) {
                    $aOperations[] = array(
                        'column' => $column,
                        'op' => 'sum',
                        'id' => 'sum'.$column
                    );
                    if(!$headerDone) {
                        $aHeader['sum_'.$emCode] = sprintf($this->translate("Sum: %s"), $questionListData['title']);
                    }
                }
                if(in_array($column,$this->questionsExportAverage)) {
                    $aOperations[] = array(
                        'column' => $column,
                        'op' => 'average',
                        'id' => 'average'.$column
                    );
                    if(!$headerDone) {
                        $aHeader['average_'.$emCode] = sprintf($this->translate("Average: %s"), $questionListData['title']);
                    }
                }
                if(in_array($column,$this->questionsExportAverageWeighted)) {
                    $aOperations[] = array(
                        'column' => $column,
                        'op' => 'weighted',
                        'id' => 'weighted'.$column
                    );
                    if(!$headerDone) {
                        $aHeader['weighted_'.$emCode] = sprintf($this->translate("Weighted average: %s"), $questionListData['title']);
                    }
                }
                if(in_array($column,$this->questionsExportAverageWeightedAlt)) {
                    $aOperations[] = array(
                        'column' => $column,
                        'op' => 'weightedalt',
                        'id' => 'weightedalt'.$column
                    );
                    if(!$headerDone) {
                        $aHeader['weightedalt_'.$emCode] = sprintf($this->translate("Alternate weighted average: %s"), $questionListData['title']);
                    }
                }
            }
        }
        $this->startExport($aHeader);
        /* orderBy part */
        $aOrderBy = array();
        if($this->questionCompileBy) {
            $aOrderBy[] = Yii::app()->getDb()->quoteColumnName($this->questionCompileBy);
        }
        if($this->questionCompile) {
            $aOrderBy[] = Yii::app()->getDb()->quoteColumnName($this->questionCompile);
        }
        $submitdateQuoted = Yii::app()->getDb()->quoteColumnName('submitdate');
        $aOrderBy[] = $submitdateQuoted;
        $oCriteria = new \CDbCriteria;
        $oCriteria->select = $questionsQuotedToBeSelected;
        $oCriteria->order = join(",",$aOrderBy);
        if($this->questionCompileBy) {
            $quotedCompileBy = Yii::app()->getDb()->quoteColumnName($this->questionCompileBy);
            $oCriteria->addCondition("$quotedCompileBy IS NOT NULL and $quotedCompileBy <>''");
        }
        if($this->questionCompile) {
            $quotedCompile = Yii::app()->getDb()->quoteColumnName($this->questionCompile);
            $oCriteria->addCondition("$quotedCompile IS NOT NULL and $quotedCompile <>''");
        }
        if($this->completed) {
            $oCriteria->addCondition("$submitdateQuoted IS NOT NULL");
        }
        if($this->token) {
            $oCriteria->compare('token',$this->token);
        }
        foreach($this->aFilters as $column => $value) {
            $quotedColumn = Yii::app()->getDb()->quoteColumnName($column);
            $oCriteria->compare($quotedColumn, $value);
        }
        if(!empty($this->responseIds)) {
            $oCriteria->addInCondition("id",$this->responseIds);
        }
        if($this->none) {
            $oCriteria->addCondition("id IS NULL");
        }

        $ResponseModel = \Response::model($this->surveyId);
        $oResponses = $ResponseModel->findAll($oCriteria);
        $aDatas = array();
        $currentCompileBy = null;
        $currentCompile = null;
        $aCurrentData = array();
        $totalCount = 0;
        $totalWeight = 0;
        $totalWeightAlt = 0;
        $optionsAllTotal = $this->options['alltotal'];
        switch ($optionsAllTotal) {
            case 'Y' :
                $setAllTotal = true;
                break;
            case 'N' :
                $setAllTotal = false;
                break;
            case 'A' :
            default :
                $setAllTotal = !empty($this->questionsExportCount)
                    || !empty($this->questionsExportSum)
                    || !empty($this->questionsExportAverage) 
                    || !empty($this->questionsExportAverageWeighted) 
                    || !empty($this->questionsExportAverageWeightedAlt);
                break;
        }
        if($setAllTotal) {
            $aAllData = array();
            $totalAllCount = 0;
            $totalAllWeight = 0;
            $totalAllWeightAlt = 0;
            $compileById = $this->questionCompileBy;
        }
        foreach($oResponses as $oResponse) {
            $newCompileBy = null;
            $newCompile = null;
            if($this->questionCompileBy) {
                $newCompileBy = $oResponse->getAttribute($this->questionCompileBy);
            }
            if($this->questionCompile) {
                $newCompile = $oResponse->getAttribute($this->questionCompile);
            }
            if($newCompileBy != $currentCompileBy || $newCompile != $currentCompile) {
                if(!empty($aCurrentData)) {
                    $aCurrentData = $this->updateDataForExport($aCurrentData, $aOperations, $totalCount, $totalWeight, $totalWeightAlt);
                    $this->addExportData($aCurrentData);
                }
                // reset
                $currentCompileBy = $newCompileBy;
                $currentCompile = $newCompile;
                $aCurrentData = array();
                $totalCount = 0;
                $totalWeight = 0;
                $totalWeightAlt = 0;
            }
            
            $weight = 1;
            $weightAlt = 1;
            if($this->questionExportWeight) {
                $weight = floatval($oResponse->getAttribute($this->questionExportWeight));
            }
            if($this->questionExportWeightAlt) {
                $weightAlt = floatval($oResponse->getAttribute($this->questionExportWeightAlt));
            }
            $totalCount++;
            $totalWeight += $weight;
            $totalWeightAlt += $weightAlt;
            if($setAllTotal) {
                $totalAllCount++;
                $totalAllWeight += $weight;
                $totalAllWeightAlt += $weightAlt;
            }
            foreach($aOperations as $aOperation) {
                if(!isset($aCurrentData[$aOperation['id']])) {
                    $aCurrentData[$aOperation['id']] = null;
                }
                if($setAllTotal && !isset($aAllData[$aOperation['id']])) {
                    $aAllData[$aOperation['id']] = null;
                }
                $value = $oResponse->getAttribute($aOperation['column']);
                switch($aOperation['op']) {
                    case 'data':
                        if (!isset($aCurrentData[$aOperation['id']])) {
                            $aCurrentData[$aOperation['id']] = $value;
                        } elseif( $aCurrentData[$aOperation['id']] != $value) {
                            $aCurrentData[$aOperation['id']] = $this->options['NATextValue'];
                        }
                        if ($setAllTotal) {
                            if (!isset($aAllData[$aOperation['id']])) {
                                $aAllData[$aOperation['id']] = $value;
                            } elseif( $aAllData[$aOperation['id']] != $value) {
                                $aAllData[$aOperation['id']] = $this->options['NATextValue'];
                            }
                        }
                        break;
                    case 'count':
                        if(!empty($value)) {
                            $aCurrentData[$aOperation['id']]++;
                            if($setAllTotal) {
                                $aAllData[$aOperation['id']]++;
                            }
                        }
                        break;
                    case 'sum':
                        $aCurrentData[$aOperation['id']]+= floatval($value);
                        if($setAllTotal) {
                            $aAllData[$aOperation['id']]+= floatval($value);
                        }
                        break;
                    case 'average':
                        $aCurrentData[$aOperation['id']]+= floatval($value);
                        if($setAllTotal) {
                            $aAllData[$aOperation['id']]+= floatval($value);
                        }
                        break;
                    case 'weighted':
                        $aCurrentData[$aOperation['id']]+= floatval($value)*$weight;
                        if($setAllTotal) {
                            $aAllData[$aOperation['id']]+= floatval($value)*$weight;
                        }
                        break;
                    case 'weightedalt':
                        $aCurrentData[$aOperation['id']]+= floatval($value)*$weightAlt;
                        if($setAllTotal) {
                            $aAllData[$aOperation['id']]+= floatval($value)*$weightAlt;
                        }
                        break;
                    default:
                        // Do nothing …
                }
            }
        }
        if (!empty($aCurrentData)) {
            $aCurrentData = $this->updateDataForExport($aCurrentData,$aOperations,$totalCount, $totalWeight, $totalWeightAlt);
            $this->addExportData($aCurrentData);
        }
        if ($setAllTotal) {
            if(!empty($aAllData)) {
                if(isset($aAllData['data'.$this->questionCompileBy])) {
                    $aAllData['data'.$this->questionCompileBy] = gT("Total");
                }
                if(isset($aAllData['data'.$this->questionCompile])) {
                    $aAllData['data'.$this->questionCompile] = gT("Total");
                }
                $aAllData = $this->updateDataForExport($aAllData,$aOperations,$totalAllCount, $totalAllWeight, $totalAllWeightAlt);
                $this->addExportData($aAllData);
            }
            if(empty($aAllData)) {
                // construct false data
                foreach($aOperations as $aOperation) {
                    switch ($aOperation['op']) {
                        case 'data':
                            $aAllData[$aOperation['id']] = $this->options['NotsetTextValue'];
                            break;
                        case 'sum':
                        case 'weighted':
                        case 'weightedalt':
                            $aAllData[$aOperation['id']] = 0;
                            break;
                        default:
                            $aAllData[$aOperation['id']] = '';
                    }
                }
                if(isset($aAllData['data'.$this->questionCompileBy])) {
                    $aAllData['data'.$this->questionCompileBy] = gT("Total");
                }
                if(isset($aAllData['data'.$this->questionCompile])) {
                    $aAllData['data'.$this->questionCompile] = gT("Total");
                }
                $aAllData = $this->updateDataForExport($aAllData,$aOperations, 0, 0, 0);
                $this->addExportData($aAllData);
            }
        }
        return $this->endExport();
    }

    /**
     * set value for a single question param
     * @param string $type value to set
     * @para string $code
     * @return void
     */
    public function setQuestionByType($type, $code) {
        if($code) {
            $this->$type = $this->getQuestionFromCode($code,$type);
        }
    }

    /**
     * set valuse for a questions param
     * @param string $type value to set
     * @param string[]
     * @return void
     */
    public function setQuestionsByType($type, $aCodes) {
        foreach($aCodes as $code) {
            if($column = $this->getQuestionFromCode($code,$type)) {
                $this->$type[] = $column;
            }
        }
    }

    /**
     * set valuse for a questions param
     * @param string $code of question
     * @param string $value
     * @return void
     */
    public function addFilter($code, $value) {
        if($column = $this->getQuestionFromCode($code, 'filter')) {
            $this->aFilters[$column] = $value;
        }
    }

    /**
     * set value to questioncompile
     * @param string
     * @return void
     */
    public function setQuestionCompile($code) {
        $this->setQuestionByType('questionCompile', $code);
    }

    /**
     * set value to questionCompileBy
     * @param string
     * @return void
     */
    public function setQuestionCompileBy($code) {
        $this->setQuestionByType('questionCompileBy', $code);
    }

    /**
     * Set the answers list for export data
     * @param string[]
     * @return void
     */
    private function setQuestionsExportDataAnswers($questionsExportData) {
        if($this->exportAnswers == 'code') {
            return;
        }
        $columnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($this->surveyId,$this->language);
        $allQuestionsColumns = $columnsInformation->allQuestionsColumns();
        foreach($questionsExportData as $column) {
            if(isset($allQuestionsColumns[$column]['filter'])) {
                $this->questionsExportDataAnswers[$column] = $allQuestionsColumns[$column]['filter'];
            }
        }
    }

    /**
     * set value to questionsExportCount
     * @param string[]
     * @return void
     */
    public function setQuestionsExportData($aCodes) {
        $this->setQuestionsByType('questionsExportData', $aCodes);
    }

    /**
     * set value to questionsExportCount
     * @param string[]
     * @return void
     */
    public function setQuestionsExportCount($aCodes) {
        $this->setQuestionsByType('questionsExportCount', $aCodes);
    }

    /**
     * set value to questionsExportSum
     * @param string[]
     * @return void
     */
    public function setQuestionsExportSum($aCodes) {
        $this->setQuestionsByType('questionsExportSum', $aCodes);
    }

    /**
     * set value to questionsExportAverage
     * @param string[]
     * @return void
     */
    public function setQuestionsExportAverage($aCodes) {
        $this->setQuestionsByType('questionsExportAverage', $aCodes);
    }

    /**
     * set value to questionsExportAverageWeighted
     * @param string[]
     * @return void
     */
    public function setQuestionsExportAverageWeighted($aCodes) {
        $this->setQuestionsByType('questionsExportAverageWeighted', $aCodes);
    }

    /**
     * set value to questionsExportAverageWeighted
     * @param string[]
     * @return void
     */
    public function setQuestionsExportAverageWeightedAlt($aCodes) {
        $this->setQuestionsByType('questionsExportAverageWeightedAlt', $aCodes);
    }

    /**
     * set value to questionExportWeight
     * @param string
     * @return void
     */
    public function setQuestionExportWeight($code) {
        $this->setQuestionByType('questionExportWeight', $code);
    }

    /**
     * set value to questionExportWeight
     * @param string
     * @return void
     */
    public function setQuestionExportWeightAlt($code) {
        $this->setQuestionByType('questionExportWeightAlt', $code);
    }

    /**
     * Start export
     * @param $aHeader
     */
    protected function startExport($aHeader) {
        $this->aHeader = $aHeader;
    }

    /**
     * Add data line
     * @param string[] $aData
     */
    protected function addExportData($aData) {
        switch ($this->dataHeader) {
            case 'text':
                $this->aDatas[] = array_combine( $this->aHeader , $aData );
                break;
            case 'none':
                $this->aDatas[] = array_values($aData );
                break;
            case 'code':
            default:
                $this->aDatas[] = array_combine( array_keys($this->aHeader) , $aData );
        }
    }

    /**
     * Set the operation
     * @param string[] $aData
     * @param array[] $aOperations
     * @param float $weigth
     * @return string[]
     */
    protected function updateDataForExport($aData,$aOperations, $count, $weight, $weightAlt)
    {
        foreach($aOperations as $aOperation) {
            switch($aOperation['op']) {
                case 'average':
                    if($count > 0 ) {
                        $aData[$aOperation['id']] = $aData[$aOperation['id']] / $count;
                    } else {
                        $aData[$aOperation['id']] = $this->options['NATextValue'];
                    }
                    break;
                case 'weighted':
                    if($weight > 0 ) {
                        $aData[$aOperation['id']] = $aData[$aOperation['id']] / $weight;
                    } else {
                        $aData[$aOperation['id']] = $this->options['NATextValue'];
                    }
                    break;
                case 'weightedalt':
                    if($weightAlt > 0 ) {
                        $aData[$aOperation['id']] = $aData[$aOperation['id']] / $weightAlt;
                    } else {
                        $aData[$aOperation['id']] = $this->options['NATextValue'];
                    }
                    break;
                case 'data':
                    $value = $aData[$aOperation['id']];
                    if(isset($this->questionsExportDataAnswers[$aOperation['column']][$value])) {
                        $aData[$aOperation['id']] = $this->questionsExportDataAnswers[$aOperation['column']][$value];
                    }
                    if(is_null($value)) {
                        $aData[$aOperation['id']] = $this->options['NotsetTextValue'];
                    }
                    break;
                case 'count':
                case 'sum':
                default:
                    $value = $aData[$aOperation['id']];
                    if(is_null($value)) {
                        $aData[$aOperation['id']] = $this->options['NotsetTextValue'];
                    }
                    break;
            }
        }
        return $aData;
    }

    /**
     * End the export
     * return array|void
     */
    protected function endExport()
    {
        return $this->aDatas;
    }

    /**
     * set value to question compile
     * @param string
     * @param string : for error log
     * @throw error
     * @return void
     */
    protected function getQuestionFromCode($code, $param) {
        $column = array_search($code,$this->aQuestionColumns);
        if(!$column) {
            $message = "Unable to find question $code in survey {$this->surveyId} ($param)";
            $this->log($message , 'error');
            if($this->throwError) {
                throw new \CHttpException(500,$message);
            }
        }
        return $column;
    }

    /**
     * translation, since we can not use gT easily 
     * @param $string to translate
     * @param string $sEscapeMode
     * @param string $language
     * @return string
     */
    protected function translate($string, $language = null)
    {
        $translated = \Yii::t(
            '',
            $string,
            array(),
            $this->languageSource,
            $language
        );
        if($this->languageSource && $string === $translated) {
            $translated = \Yii::t(
                '',
                $string,
                array(),
                null,
                $language
            );
        }
        return $translated;
    }

    /**
    * @inheritdoc adding string, by default current event
    * @param string
    */
    public function log($message, $level = \CLogger::LEVEL_TRACE,$logDetail = null)
    {
        Yii::log($message, $level,'plugin.filteredAdaptedExport.helpers.'.get_class($this).'.'.$logDetail);
    }
}
