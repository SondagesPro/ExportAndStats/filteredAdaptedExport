<?php
/**
 * Description
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 1.0.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
namespace filteredAdaptedExport\exports;

use Yii;
use CException;
use Survey;

class filteredExportJson extends filteredExport
{

    /**
     * Add data line
     * @param string[] $aData
     * @param array[] $aOperations
     * @param float $weigth
     */
    protected function addExportData($aData) {
        if($this->exportHeader == 'text') {
            $this->aDatas[] = array_combine( $this->aHeader , $aData );
            return;
        }
        parent::addExportData($aData);
    }

    /**
     * End the export
     * return array|void
     */
    protected function endExport()
    {
        Yii::import('application.helpers.viewHelper');
        \viewHelper::disableHtmlLogging();
        header('Content-Type: application/json');
        echo ls_json_encode($this->aDatas);
        Yii::app()->end();
    }

}
