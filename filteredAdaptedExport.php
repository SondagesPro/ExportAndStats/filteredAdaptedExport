<?php
/**
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2022 Denis Chenu <http://www.sondages.pro>
 * @copyright 2020-2021 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license GPL
 * @version 1.4.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class filteredAdaptedExport extends PluginBase
{
    protected $storage = 'DbStorage';

    private static $version = "1.1.2";

    protected static $description = 'Filtered and compiled export system.';
    protected static $name = 'filteredAdaptedExport';

    /* @inheritdoc 2 public methods */
    public $allowedPublicMethods = array(
        'actionSettings',
        'actionSaveSettings',
    );

    public function init()
    {
        $this->subscribe('beforeToolsMenuRender');
        $this->subscribe('newDirectRequest', 'exportDataAs');
        $this->subscribe('afterPluginLoad');
    }

    /** Add the alias */
    public function afterPluginLoad()
    {
        Yii::setPathOfAlias("filteredAdaptedExport", dirname(__FILE__));
    }
    /**
     * see beforeToolsMenuRender event
     * @deprecated ? See https://bugs.limesurvey.org/view.php?id=15476
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aMenuItem = array(
            'label' => $this->translate('Filtered export'),
            'iconClass' => 'fa fa-calendar-check-o',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionSettings',
                    'surveyId' => $surveyId
                )
            ),
        );
        $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        $event->append('menuItems', array($menuItem));
    }

    /**
     * Save the settings
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionSaveSettings($surveyId)
    {
        if (empty(App()->getRequest()->getPost('save'.get_class($this)))) {
            throw new CHttpException(400);
        }
        $oSurvey=Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $aInputs = array(
            'questionCompile',
            'questionsCompile',
            'questionCompileBy',
            'extraFilterQuestion',
            'extraFilterValue',
            'extraFilterValueByUrl',
            'questionsExportData',
            'questionsExportCount',
            'questionsExportSum',
            'questionsExportAverage',
            'questionExportWeight',
            'questionsExportAverageWeighted',
            'questionExportWeightAlt',
            'questionsExportAverageWeightedAlt',
            'addTotalLine'
        );
        foreach($aInputs as $input) {
            $this->set(
                $input,
                App()->getRequest()->getPost($input),
                'Survey',
                $surveyId
            );
        }
        if (App()->getRequest()->getPost('save'.get_class($this)) == 'redirect') {
            $redirectUrl = Yii::app()->createUrl('surveyAdministration/view', array('surveyid' => $surveyId));
            if (intval(App()->getConfig('versionnumber')) < 4) {
                $redirectUrl = Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId));
            }
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $redirectUrl = Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId));
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }

    /**
     * Construct and show settings
     * @param int $surveyId Survey id
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $this->fixVersion();
        $oSurvey=Survey::model()->findByPk($surveyId);
        $language = $oSurvey->language;
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'read')) {
            throw new CHttpException(403);
        }
        if (intval(App()->getConfig('versionnumber')) < 4) {
            return $this->actionSettings3LTS($surveyId);
        }
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['lang'] = array(
            "Filtered and compliled export" => $this->translate("Filtered and compliled export")
        );
        $aSettings=array();
        /* Plugion check */
        if(!Yii::getPathOfAlias('getQuestionInformation')) {
            $error = sprintf($this->translate("You need %sgetQuestionInformation%s plugin to export the data."),"<a href='https://gitlab.com/SondagesPro/coreAndTools/getQuestionInformation' rel='external'>","</a>");
            $aSettings[$this->translate('Unable to use this plugin')] = array(
                'link'=>array(
                    'type'=>'info',
                    'content'=> "<div class='alert alert-danger h4'>".$error."</div>"
                ,
                ),
            );
        }
        /* Link if valid */
        $disabled = $oSurvey->active != "Y";
        $exportUrl = Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'sid'=>$surveyId));
        $linkExport = CHtml::link(
            $this->translate("Export completed data with current params"),
            $exportUrl,array("target"=>'_blank','class'=>'btn btn-block btn-default','disabled'=>$disabled)
        );
        $exportAllUrl = Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'sid'=>$surveyId,'state'=>'all'));
        $linkAllExport = CHtml::link(
            $this->translate("Export all data with current params"),
            $exportAllUrl,array("target"=>'_blank','class'=>'btn btn-block btn-default','disabled'=>$disabled)
        );
        $exportNoCompileUrl = Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'sid'=>$surveyId,'compile'=>'0'));
        $linkExportNoCompile = CHtml::link(
            $this->translate("Export data without compiled"),
            $exportNoCompileUrl,array("target"=>'_blank','class'=>'btn btn-block btn-default','disabled'=>$disabled)
        );
        if($this->isActive($surveyId)) {
            $aSettings[$this->translate('Export links')] = array(
                'link'=>array(
                    'type'=>'info',
                    'content'=> $linkExport.$linkAllExport.$linkExportNoCompile,
                ),
            );
        }
        /* The single choice question for compilation */
        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId,App()->getLanguage());
        $surveyColumnsInformation->ByEmCode = true;
        /* Available questions for match */
        $surveyColumnsInformation->restrictToType = ['L','!','*','N','S'];
        $singleChoiceQuestions = $surveyColumnsInformation->allQuestionListData();
        $doc = "<ul>";
        $doc .= "<li>".$this->translate("If the value is not set : response was not exported.")."</li>";
        $doc .= "<li>".$this->translate("If the value was set and there are only one response : response was exported without update.")."</li>";
        $doc .= "<li>".$this->translate("If the value was set and there are multiple responses : responses was compiled.")."</li>";
        $doc .= "<ul>";
        $singleChoiceOptions = $singleChoiceQuestions['data'];
        $singleChoiceHtmlOptions = $singleChoiceQuestions['options'];
        $aQuestionCompile = array(
            'questionCompileDoc' => array(
                'type' => 'info',
                'content' => '<div class="well">'.$doc.'</div>',
            ),
            'questionCompile' => array(
                'type'=>'select',
                'htmlOptions'=>array(
                    'empty'=>gT("Id"),
                    'options' => $singleChoiceHtmlOptions,
                ),
                'label'=>$this->translate("Default question for filter and compile"),
                'options' => $singleChoiceOptions,
                'current'=>$this->get('questionCompile', 'Survey', $surveyId, null),
                'help' => "",
            ),
            'questionsCompile' => array(
                'type'=>'select',
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options' => $singleChoiceHtmlOptions,
                ),
                'label' => $this->translate("Other available question for filter and compile"),
                'options' => $singleChoiceOptions ,
                'current' => $this->get('questionsCompile', 'Survey', $surveyId, null),
                'help' => $this->translate("This was checked in <code>compile</code> url param."),
            ),
            'questionCompileBy' => array(
                'type'=>'select',
                'htmlOptions'=>array(
                    'empty'=>$this->translate("None"),
                    'options' => $singleChoiceHtmlOptions,
                ),
                'label'=>$this->translate("Question for separation of compilation (compilation by)"),
                'options'=> $singleChoiceOptions,
                'current'=>$this->get('questionCompileBy', 'Survey', $surveyId, null),
                'help' => "",
            ),
            'extraFilterQuestion' => array(
                'type' => 'select',
                'htmlOptions'=>array(
                    'empty'=>$this->gT("None"),
                    'options' => $singleChoiceHtmlOptions,
                ),
                'label'=>$this->translate("Extra filter question"),
                'help'=>$this->translate("Used only when download via direct link."),
                'options'=> $singleChoiceOptions,
                'current' => $this->get('extraFilterQuestion', 'Survey', $surveyId, null),
            ),
            'extraFilterValue' => array(
                'type' => 'string',
                'label' => $this->translate("Filter value"),
                'current' => $this->get('extraFilterValue', 'Survey', $surveyId, ''),
            ),
            'extraFilterValueByUrl' => array(
                'type'=>'select',
                'label'=>$this->translate("Can replace filter value in URL"),
                'options'=>array(
                    1 =>gT("Yes"),
                    0 =>gT("No"),
                ),
                'current'=>$this->get('extraFilterValueByUrl', 'Survey', $surveyId, 0),
                'help' => $this->translate("User can set another value using <code>filter</code> parameters in url."),
            ),
            'allowAllByUrl' => array(
                'type'=>'select',
                'label'=>$this->translate("Allow force all by parameter in URL"),
                'options'=>array(
                    1 =>gT("Yes"),
                    0 =>gT("No"),
                ),
                'current'=>$this->get('allowAllByUrl', 'Survey', $surveyId, 0),
                'help' => $this->translate("User can force to get all value using <code>all=1</code> parameters in url."),
            ),
        );
        $aSettings[$this->translate("Questions for filter and compile")] = $aQuestionCompile;
        /* Question to be exported only numeric ?  */
        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId,App()->getLanguage());
        $surveyColumnsInformation->ByEmCode = true;
        $aQuestionList = $surveyColumnsInformation->allQuestionListData();
        $aQuestionExport = array(
            'questionsExportData' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported as value'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportData','Survey',$surveyId),
                'help' => $this->translate("This export the value of the first answer set"),
            ),
            'questionsExportCount' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported and compiled as count.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportCount','Survey',$surveyId)
            ),
            'questionsExportSum' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported and compile as sum.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportSum','Survey',$surveyId)
            ),
            'questionsExportAverage' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported and compile as average.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportAverage','Survey',$surveyId)
            ),
            'questionExportWeight' => array(
                'type'=>'select',
                'label'=>$this->translate('Question for weighted average.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>false,
                    'empty'=>$this->translate("None"),
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover',
                ),
                'current'=>$this->get('questionExportWeight','Survey',$surveyId)
            ),
            'questionsExportAverageWeighted' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported and compile as weighted average.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportAverageWeighted','Survey',$surveyId)
            ),
            'questionExportWeightAlt' => array(
                'type'=>'select',
                'label'=>$this->translate('Question for alternate weighted average.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>false,
                    'empty'=>$this->translate("None"),
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover',
                ),
                'current'=>$this->get('questionExportWeightAlt','Survey',$surveyId)
            ),
            'questionsExportAverageWeightedAlt' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported and compile as alternate weighted average.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportAverageWeightedAlt','Survey',$surveyId)
            ),
            'addTotalLine' => array(
                'type' => 'select',
                'label' => $this->translate('Add total line.'),
                'options' => array(
                    'Y' => gT("Yes"),
                    'A' => gT("Automatic"),
                    'N' => gT("No"),
                ),
                'label' => $this->translate('You can enable or disable total line, automatic use currrent questions to be exported, activate only if there are a sum or average data.'),
                'current'=>$this->get('addTotalLine', 'Survey', $surveyId, 'A')
            ),
        );
        $aSettings[$this->translate("Questions to be exported")] = $aQuestionExport;

        $aRightSettings = array(
            'exportTokenRight' => array(
                'type'=>'select',
                'htmlOptions'=>array(
                    'empty' => $this->translate("No"),
                ),
                'options' => array(
                    'token' => $this->translate("Yes"), // Find all rights ?
                ),
                'label'=>$this->translate("Allow export by token"),
                'help'=>$this->translate("Administrator with export right can always export, token in URL param can be used by administrator."),
                'current'=>$this->get('exportRight', 'Survey', $surveyId, 'token'),
                
            ),
        );
        $aSettings[$this->translate("Rights")] = $aRightSettings;

        $aData['aSettings']=$aSettings;
        $aData['form'] = array(
            'action' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSaveSettings','surveyId' => $surveyId)),
            'reset' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId)),
            'close' => Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId)),
            'allowsave' => Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')
        );
        $content = $this->renderPartial('admin.settings', $aData, true);
        return $content;
    }

    /**
     * Construct and show settings
     * @param int $surveyId Survey id
     * @return string
     */
    private function actionSettings3LTS($surveyId)
    {
        $this->fixVersion();
        $oSurvey=Survey::model()->findByPk($surveyId);
        $language = $oSurvey->language;
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'read')) {
            throw new CHttpException(403);
        }
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['lang'] = array(
            "Filtered and compliled export" => $this->translate("Filtered and compliled export")
        );
        $aSettings=array();
        /* Plugion check */
        if(!Yii::getPathOfAlias('getQuestionInformation')) {
            $error = sprintf($this->translate("You need %sgetQuestionInformation%s plugin to export the data."),"<a href='https://gitlab.com/SondagesPro/coreAndTools/getQuestionInformation' rel='external'>","</a>");
            $aSettings[$this->translate('Unable to use this plugin')] = array(
                'link'=>array(
                    'type'=>'info',
                    'content'=> "<div class='alert alert-danger h4'>".$error."</div>"
                ,
                ),
            );
        }
        /* Link if valid */
        $disabled = $oSurvey->active != "Y";
        $exportUrl = Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'sid'=>$surveyId));
        $linkExport = CHtml::link(
            $this->translate("Export completed data with current params"),
            $exportUrl,array("target"=>'_blank','class'=>'btn btn-block btn-default','disabled'=>$disabled)
        );
        $exportAllUrl = Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'sid'=>$surveyId,'state'=>'all'));
        $linkAllExport = CHtml::link(
            $this->translate("Export all data with current params"),
            $exportAllUrl,array("target"=>'_blank','class'=>'btn btn-block btn-default','disabled'=>$disabled)
        );
        $exportNoCompileUrl = Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'sid'=>$surveyId,'compile'=>'0'));
        $linkExportNoCompile = CHtml::link(
            $this->translate("Export data without compiled"),
            $exportNoCompileUrl,array("target"=>'_blank','class'=>'btn btn-block btn-default','disabled'=>$disabled)
        );
        if($this->isActive($surveyId)) {
            $aSettings[$this->translate('Export links')] = array(
                'link'=>array(
                    'type'=>'info',
                    'content'=> $linkExport.$linkAllExport.$linkExportNoCompile,
                ),
            );
        }
        /* The single choice question for compilation */
        $oQuestionCriteria = new CDbCriteria();
        $oQuestionCriteria->condition = "t.sid =:sid and t.language=:language and parent_qid = 0";
        $oQuestionCriteria->params = array(":sid"=>$surveyId,":language"=>$language);
        $oQuestionCriteria->order = "group_order ASC, question_order ASC";
        $oQuestionCriteria->addInCondition("type", ['L','!','*','N','S']);
        $oaQuestion = Question::model()->with('groups')->findAll($oQuestionCriteria);
        $doc = "<ul>";
        $doc .= "<li>".$this->translate("If the value is not set : response was not exported.")."</li>";
        $doc .= "<li>".$this->translate("If the value was set and there are only one response : response was exported without update.")."</li>";
        $doc .= "<li>".$this->translate("If the value was set and there are multiple responses : responses was compiled.")."</li>";
        $doc .= "<ul>";
        $singleChoiceOptions = CHtml::listData(
            $oaQuestion,
            'title',
            function ($oQuestion) {
                return "[".$oQuestion->title."] ".viewHelper::flatEllipsizeText($oQuestion->question, 1, 40, "…");
            }
        );
        $aQuestionCompile = array(
            'questionCompileDoc' => array(
                'type' => 'info',
                'content' => '<div class="well">'.$doc.'</div>',
            ),
            'questionCompile' => array(
                'type'=>'select',
                'htmlOptions'=>array(
                    'empty'=>gT("Id"),
                ),
                'label'=>$this->translate("Default question for filter and compile"),
                'options' => $singleChoiceOptions,
                'current'=>$this->get('questionCompile', 'Survey', $surveyId, null),
                'help' => "",
            ),
            'questionsCompile' => array(
                'type'=>'select',
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    //~ 'options' => $singleChoiceOptions, // In dropdown, but not in select2
                ),
                'label' => $this->translate("Other available question for filter and compile"),
                'options' => $singleChoiceOptions ,
                'current' => $this->get('questionsCompile', 'Survey', $surveyId, null),
                'help' => $this->translate("This was checked in <code>compile</code> url param."),
            ),
            'questionCompileBy' => array(
                'type'=>'select',
                'htmlOptions'=>array(
                    'empty'=>$this->translate("None"),
                ),
                'label'=>$this->translate("Question for separation of compilation (compilation by)"),
                'options'=> $singleChoiceOptions,
                'current'=>$this->get('questionCompileBy', 'Survey', $surveyId, null),
                'help' => "",
            ),
            'extraFilterQuestion' => array(
                'type' => 'select',
                'htmlOptions'=>array(
                    'empty'=>$this->gT("None"),
                ),
                'label'=>$this->translate("Extra filter question"),
                'help'=>$this->translate("Used only when download via direct link."),
                'options'=> $singleChoiceOptions,
                'current' => $this->get('extraFilterQuestion', 'Survey', $surveyId, null),
            ),
            'extraFilterValue' => array(
                'type' => 'string',
                'label' => $this->translate("Filter value"),
                'current' => $this->get('extraFilterValue', 'Survey', $surveyId, ''),
            ),
            'extraFilterValueByUrl' => array(
                'type'=>'select',
                'label'=>$this->translate("Can replace filter value in URL"),
                'options'=>array(
                    1 =>gT("Yes"),
                    0 =>gT("No"),
                ),
                'current'=>$this->get('extraFilterValueByUrl', 'Survey', $surveyId, 0),
                'help' => $this->translate("User can set another value using <code>filter</code> parameters in url."),
            ),
            'allowAllByUrl' => array(
                'type'=>'select',
                'label'=>$this->translate("Allow force all by parameter in URL"),
                'options'=>array(
                    1 =>gT("Yes"),
                    0 =>gT("No"),
                ),
                'current'=>$this->get('allowAllByUrl', 'Survey', $surveyId, 0),
                'help' => $this->translate("User can force to get all value using <code>all=1</code> parameters in url."),
            ),
        );
        $aSettings[$this->translate("Questions for filter and compile")] = $aQuestionCompile;
        /* Question to be exported only numeric ?  */
        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId,App()->getLanguage());
        $surveyColumnsInformation->ByEmCode = true;
        $aQuestionList = $surveyColumnsInformation->allQuestionListData();
        $aQuestionExport = array(
            'questionsExportData' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported as value'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportData','Survey',$surveyId),
                'help' => $this->translate("This export the value of the first answer set"),
            ),
            'questionsExportCount' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported and compiled as count.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportCount','Survey',$surveyId)
            ),
            'questionsExportSum' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported and compile as sum.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportSum','Survey',$surveyId)
            ),
            'questionsExportAverage' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported and compile as average.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportAverage','Survey',$surveyId)
            ),
            'questionExportWeight' => array(
                'type'=>'select',
                'label'=>$this->translate('Question for weighted average.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>false,
                    'empty'=>$this->translate("None"),
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover',
                ),
                'current'=>$this->get('questionExportWeight','Survey',$surveyId)
            ),
            'questionsExportAverageWeighted' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported and compile as weighted average.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportAverageWeighted','Survey',$surveyId)
            ),
            'questionExportWeightAlt' => array(
                'type'=>'select',
                'label'=>$this->translate('Question for alternate weighted average.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>false,
                    'empty'=>$this->translate("None"),
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover',
                ),
                'current'=>$this->get('questionExportWeightAlt','Survey',$surveyId)
            ),
            'questionsExportAverageWeightedAlt' => array(
                'type'=>'select',
                'label'=>$this->translate('Question to be exported and compile as alternate weighted average.'),
                'options'=>$aQuestionList['data'],
                'htmlOptions'=>array(
                    'multiple'=>true,
                    'placeholder'=>gT("None"),
                    'unselectValue'=>"",
                    'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                ),
                'selectOptions'=>array(
                    'placeholder'=>gT("None"),
                    //~ 'templateResult'=>"formatQuestion",
                ),
                'controlOptions' => array(
                    'class'=>'select2-withover ',
                ),
                'current'=>$this->get('questionsExportAverageWeightedAlt','Survey',$surveyId)
            ),
            'addTotalLine' => array(
                'type' => 'select',
                'label' => $this->translate('Add total line.'),
                'options' => array(
                    'Y' => gT("Yes"),
                    'A' => gT("Automatic"),
                    'N' => gT("No"),
                ),
                'label' => $this->translate('You can enable or disable total line, automatic use currrent questions to be exported, activate only if there are a sum or average data.'),
                'current'=>$this->get('addTotalLine', 'Survey', $surveyId, 'A')
            ),
        );
        $aSettings[$this->translate("Questions to be exported")] = $aQuestionExport;

        $aRightSettings = array(
            'exportTokenRight' => array(
                'type'=>'select',
                'htmlOptions'=>array(
                    'empty' => $this->translate("No"),
                ),
                'options' => array(
                    'token' => $this->translate("Yes"), // Find all rights ?
                ),
                'label'=>$this->translate("Allow export by token"),
                'help'=>$this->translate("Administrator with export right can always export, token in URL param can be used by administrator."),
                'current'=>$this->get('exportRight', 'Survey', $surveyId, 'token'),
                
            ),
        );
        $aSettings[$this->translate("Rights")] = $aRightSettings;

        $aData['aSettings']=$aSettings;
        $aData['form'] = array(
            'action' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSaveSettings','surveyId' => $surveyId)),
            'reset' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId)),
            'close' => Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId)),
            'allowsave' => Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')
        );
        $content = $this->renderPartial('admin.settings', $aData, true);
        return $content;
    }


    public function exportDataAs()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getEvent()->get('target') != get_class()) {
            return;
        }
        $type = Yii::app()->getRequest()->getParam('format','xlsx');
        $header = Yii::app()->getRequest()->getParam('header','text');
        $answer = Yii::app()->getRequest()->getParam('answer','text');
        $compile = Yii::app()->getRequest()->getParam('compile',true);
        $total = Yii::app()->getRequest()->getParam('total',false);
        $filter = Yii::app()->getRequest()->getParam('filter',null);
        $surveyId = Yii::app()->getRequest()->getQuery('sid',Yii::app()->getRequest()->getQuery('surveyid'));
        $this->exportData($surveyId, $type, $header, $answer, $compile, $total);
    }

    /**
     * Export the data according to settings, permisison is check
     * can stay public : public cal allowed
     * @param integer surveyId
     * @param string $type : defauult return the array, support export file by \filteredAdaptedExport\exports\filteredExport extended function
     * @param string header : text|code
     * @param string answer : text|code
     * @param boolean compile|string : use compile column, if string : check if comile is available to use
     * @param boolean total : add a grand total
     * @return array|mixed
     */
    public function exportData($surveyId, $type = 'return', $header = 'text', $answer = 'text', $compile = true, $total = false)
    {
        if ((string) (int) $surveyId !== (string) $surveyId) {
            throw new CHttpException(403, gT("Invalid survey id"));
        }
        $oSurvey=Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("Sorry, this response was not found."));
        }
        if ($oSurvey->active != "Y") {
            throw new CHttpException(400, gT("This survey has not been activated."));
        }
        if(!$this->isActive($surveyId)) {
            throw new CHttpException(400);
        }
        if(!Yii::getPathOfAlias('getQuestionInformation')) {
            throw new CHttpException(500, gT("Need getQuestionInformation plugin."));
        }
        $bAdminExportRight = Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export');
        $token = null;
        if($oSurvey->anonymized != 'Y') {
            // Review ?
            $token = Yii::app()->getRequest()->getParam('token');
        }
        /* TODO : usage of uniqueId if exist */
        if(!$bAdminExportRight && empty($token)) {
            throw new CHttpException(401);
        }
        $lang = Yii::app()->getRequest()->getParam('lang');
        switch ($type) {
            case 'xlsx':
                $filteredExport = new \filteredAdaptedExport\exports\filteredExportXls($surveyId,$lang);
                break;
            case 'json':
                $filteredExport = new \filteredAdaptedExport\exports\filteredExportJson($surveyId,$lang);
                break;
            case 'return':
            default:
                $filteredExport = new \filteredAdaptedExport\exports\filteredExport($surveyId,$lang);
        }
        $filteredExport->throwError = Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'read');
        if($compile) {
            $questionCompile = $this->get('questionCompile', 'Survey', $surveyId);
            if(!is_int($compile) && !is_bool($compile)) {
                $questionsCompile = $this->get('questionsCompile', 'Survey', $surveyId,array());
                if(is_array($questionsCompile)) {
                    $questionsCompileLower = array_map('strtolower',$questionsCompile);
                    if(in_array(strtolower($compile), $questionsCompileLower)) {
                        $key = array_search(strtolower($compile), $questionsCompileLower);
                        $questionCompile = $questionsCompile[$key];
                    }
                }
            }
            $filteredExport->setQuestionCompile($questionCompile);
        }
        $filteredExport->setQuestionCompileBy($this->get('questionCompileBy', 'Survey', $surveyId));
        if (!empty($this->get('questionsExportData', 'Survey', $surveyId))) {
            $filteredExport->setQuestionsExportData($this->get('questionsExportData', 'Survey', $surveyId));
        }
        if (!empty($this->get('questionsExportCount', 'Survey', $surveyId))) {
            $filteredExport->setQuestionsExportCount($this->get('questionsExportCount', 'Survey', $surveyId));
        }
        if (!empty($this->get('questionsExportSum', 'Survey', $surveyId))) {
            $filteredExport->setQuestionsExportSum($this->get('questionsExportSum', 'Survey', $surveyId));
        }
        if (!empty($this->get('questionsExportAverage', 'Survey', $surveyId))) {
            $filteredExport->setQuestionsExportAverage($this->get('questionsExportAverage', 'Survey', $surveyId));
        }
        if (!empty($this->get('questionsExportAverageWeighted', 'Survey', $surveyId))) {
            $filteredExport->setQuestionsExportAverageWeighted($this->get('questionsExportAverageWeighted', 'Survey', $surveyId));
        }
        if (!empty($this->get('questionsExportAverageWeightedAlt', 'Survey', $surveyId))) {
            $filteredExport->setQuestionsExportAverageWeightedAlt($this->get('questionsExportAverageWeightedAlt', 'Survey', $surveyId));
        }
        $filteredExport->setQuestionExportWeight($this->get('questionExportWeight', 'Survey', $surveyId));
        $filteredExport->setQuestionExportWeightAlt($this->get('questionExportWeightAlt', 'Survey', $surveyId));
        $filteredExport->token = $token;
        if (!empty($this->get('extraFilterQuestion', 'Survey', $surveyId))) {
            $value = $this->get('extraFilterValue', 'Survey', $surveyId);
            if(!empty(Yii::app()->getRequest()->getParam('filter')) && $this->get('extraFilterValueByUrl', 'Survey', $surveyId)) {
                $value = Yii::app()->getRequest()->getParam('filter');
            }
            $filteredExport->addFilter($this->get('extraFilterQuestion', 'Survey', $surveyId), $value);
        }
        if($this->get('allowAllByUrl', 'Survey', $surveyId, 0)) {
            if(Yii::app()->getRequest()->getParam('all')) {
                $filteredExport->completed = false;
            }
        }
        $filteredExport->options['alltotal'] = $this->get('addTotalLine', 'Survey', $surveyId, 'A');
        $filteredExport->exportHeader = $header;
        $filteredExport->exportAnswers = $answer;

        $aData = $filteredExport->export();
        if($type == 'return') {
            return $aData;
        }
        Yii::app()->end();
    }


    /**
     * Get all fixed settings
     * @param $surveyId
     * @return array[]
     */
    private function getAllQuestionsSettings($surveyId)
    {
        $bAdminSettingsRight = Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'read');
        
        $questionsSettings = array(
            'questionCompileBy' => false,
            'questionsExportData' => array(),
            '' => array(),
            '' => array(),
            '' => array(),
            '' => false,
        );
        $params = array('questionCompile','questionCompileBy','questionExportWeight');

        $params = array('questionsExportData','questionsExportCount','questionsExportSum','questionsExportAverage');
        foreach($params as $param)
        {
            $questionsCode = $this->get($param, 'Survey', $surveyId);
            if(!empty($questionsCode)) {
                foreach($questionsCode as $questionCode) {
                    $questionColumn = array_search($questionCode,$aQuestionColumns);
                    if(!$questionColumn) {
                        $message = "Unable to find question $questionColumn in survey $surveyId ($param)";
                        $this->log($message , 'error');
                        if($bAdminSettingsRight) {
                            throw new CHttpException(500,$message);
                        }
                    } else {
                        $questionsSettings[$param][] = $questionColumn;
                    }
                }
            }
        }

        return $questionsSettings;
    }

    /**
     * Get if activated for current surey
     * @param integer surveyid
     * @return boolean
     */
    public function isActive($surveyId)
    {
        if(!empty($this->get('questionsExportData', 'Survey', $surveyId)) ) {
            return true;
        }
        if(!empty($this->get('questionsExportCount', 'Survey', $surveyId)) ) {
            return true;
        }
        if(!empty($this->get('questionsExportSum', 'Survey', $surveyId)) ) {
            return true;
        }
        if(!empty($this->get('questionsExportAverage', 'Survey', $surveyId)) ) {
            return true;
        }
    }
    /**
     * Translation replacer
     * @param string $string to b translated
     * @param string $sEscapeMode default to unescaped
     * @param string $sLanguage for translation , default to current Yii lang
     * @return string translated string
     */
    private function translate($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return $this->gT($string, $sEscapeMode, $sLanguage);
    }

    /**
     * Fix version in db
     */
    private function fixVersion()
    {
        $oPlugin = Plugin::model()->find(
            "name = :name",
            array(":name" => "filteredAdaptedExport")
        );
        if(!$oPlugin) {
            return;
        }
        if($oPlugin->version && $oPlugin->version == self::$version ) {
            return;
        }
        $oPlugin->version = self::$version;
        $oPlugin->save();
    }
    /**
    * @inheritdoc adding string, by default current event
    * @param string
    */
    public function log($message, $level = \CLogger::LEVEL_TRACE,$logDetail = null)
    {
        if(!$logDetail && $this->getEvent()) {
            $logDetail = $this->getEvent()->getEventName();
        } // What to put if no event ?
        if(intval(Yii::app()->getConfig('versionnumber')) < 3) {
            Yii::log($message, $level,'plugin.'.get_class($this).'.'.$logDetail);
            return;
        }
        parent::log($message, $level);
    }
}
